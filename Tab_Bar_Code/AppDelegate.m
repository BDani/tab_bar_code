//
//  AppDelegate.m
//  Tab_Bar_Code
//
//  Created by Bruno Tavares on 23/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.tabBarController = [[UITabBarController alloc] init];
    
    ViewController *viewController1 = [[ViewController alloc] init];
    viewController1.labelString = @"1";
    ViewController *viewController2 = [[ViewController alloc] init];
    viewController2.labelString = @"2";
    ViewController *viewController3 = [[ViewController alloc] init];
    viewController3.labelString = @"3";
    ViewController *viewController4 = [[ViewController alloc] init];
    viewController4.labelString = @"4";
    ViewController *viewController5 = [[ViewController alloc] init];
    viewController5.labelString = @"5";
    ViewController *viewController6 = [[ViewController alloc] init];
    viewController6.labelString = @"6";
    
    viewController1.title = @"View 1";
    viewController2.title = @"View 2";
    viewController3.title = @"View 3";
    viewController4.title = @"View 4";
    viewController5.title = @"View 5";
    viewController6.title = @"View 6";
    
    //viewController1.tabBarItem.image = [UIImage imageNamed:@"4337686_G6U2H.png"];
    //viewController2.tabBarItem.image = [UIImage imageNamed:@"4337681_vozr8.png"];
    
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:viewController1, viewController2, viewController3, viewController4, viewController5, viewController6, nil];
    
    [self.tabBarController setSelectedIndex:0];
    
    self.window.rootViewController = self.tabBarController;
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
