//
//  ViewController.m
//  Tab_Bar_Code
//
//  Created by Bruno Tavares on 23/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // configure label
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 75, 200)];
    self.label.font = [UIFont boldSystemFontOfSize:120];
    self.label.textColor = [UIColor blackColor];
    self.label.text = self.labelString;
    
    [self.view addSubview:self.label];
    
    // auto-layout configuration
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.view
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:0
                                     toItem:self.label
                                  attribute:NSLayoutAttributeCenterY
                                 multiplier:1
                                   constant:0]];
    
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.view
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:0
                                     toItem:self.label
                                  attribute:NSLayoutAttributeCenterX
                                 multiplier:1
                                   constant:0]];
}

@end
